package com.empik.recruitment.repository.login_counter;

import com.empik.recruitment.model.entity.LoginCounter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoginCounterDaoImplTest {

    @Mock
    private LoginCounterRepository loginCounterRepository;

    @InjectMocks
    private LoginCounterDaoImpl loginCounterDaoImpl;

    private LoginCounter loginCounter;

    @BeforeEach
    public void init() {
        loginCounter = LoginCounter.builder()
                .login("testLogin")
                .requestCount(30)
                .build();
    }

    @Test
    public void shouldIncrementLoginCounter() {
        when(loginCounterRepository.findByLogin(anyString())).thenReturn(Optional.ofNullable(loginCounter));
        loginCounterDaoImpl.updateLoginCounter(loginCounter.getLogin());
        assertEquals(loginCounter.getRequestCount(), 31);
    }
}