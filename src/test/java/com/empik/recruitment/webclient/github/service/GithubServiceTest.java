package com.empik.recruitment.webclient.github.service;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class GithubServiceTest {

    @ParameterizedTest
    @ValueSource(ints = {-5, 0, 5})
    public void shouldReturnCalculationResult(int testValue) {
        GithubService githubService = new GithubService();
        float result = githubService.performCalculations(testValue, testValue);
        assertThat(result).isBetween(0.0f, 9.0f);
    }
}