package com.empik.recruitment.controller;

import com.empik.recruitment.model.dto.UserDto;
import com.empik.recruitment.service.LoginCounterService;
import com.empik.recruitment.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
class UserControllerTest {

    @MockBean
    private UserService userService;

    @MockBean
    private LoginCounterService loginCounterService;

    @Autowired
    private MockMvc mockMvc;

    private UserDto userDto;
    private final String usersUrl = "/users/";

    @BeforeEach
    public void init() throws ParseException {
        Instant createdTime = new SimpleDateFormat("yyyy-MM-dd").parse("2021-01-31").toInstant();
        userDto = UserDto.builder()
                .id(1)
                .login("testLogin")
                .name("testName")
                .type("testType")
                .avatarUrl("testUrl")
                .createdAt(createdTime)
                .calculations(Float.MAX_VALUE)
                .build();
    }

    @Test
    public void shouldReturnUserByLogin() throws Exception {
        when(userService.getUserByLogin(anyString())).thenReturn(userDto);

        mockMvc.perform(get(usersUrl + userDto.getLogin()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(userDto.getName()))
                .andExpect(jsonPath("$.calculations").value(userDto.getCalculations()))
                .andDo(print());

        verify(userService, times(1)).getUserByLogin(anyString());
        verify(loginCounterService, times(1)).incrementLoginCounter(anyString());
    }
}