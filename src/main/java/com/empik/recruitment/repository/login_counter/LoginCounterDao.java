package com.empik.recruitment.repository.login_counter;

public interface LoginCounterDao {
    void updateLoginCounter(String login);
}
