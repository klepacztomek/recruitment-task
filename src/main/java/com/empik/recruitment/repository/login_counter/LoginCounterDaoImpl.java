package com.empik.recruitment.repository.login_counter;

import com.empik.recruitment.model.entity.LoginCounter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class LoginCounterDaoImpl implements LoginCounterDao {

    private final LoginCounterRepository loginCounterRepository;

    @Override
    public void updateLoginCounter(String login) {
        log.debug("Invoked update login counter with login: " + login);
        Optional<LoginCounter> loginCounter = loginCounterRepository.findByLogin(login);

        if (loginCounter.isPresent()) {
            loginCounter.get().setRequestCount(loginCounter.get().getRequestCount() + 1);
            loginCounterRepository.save(loginCounter.get());
        } else {
            LoginCounter newLoginCounter = LoginCounter.builder()
                    .login(login)
                    .requestCount(1)
                    .build();
            loginCounterRepository.save(newLoginCounter);
        }
    }
}
