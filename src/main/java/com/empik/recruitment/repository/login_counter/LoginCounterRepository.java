package com.empik.recruitment.repository.login_counter;

import com.empik.recruitment.model.entity.LoginCounter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoginCounterRepository extends CrudRepository<LoginCounter, Long> {
    Optional<LoginCounter> findByLogin(String login);
}
