package com.empik.recruitment.exceptions;

import com.empik.recruitment.exceptions.client.CustomClientResponseException;
import lombok.Getter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
public class CustomErrorResponse {
    private int statusCode;
    private String error;
    private String errorDetails;
    private String timestamp;

    public CustomErrorResponse(CustomClientResponseException exception) {
        this.statusCode = exception.getStatusCode().value();
        this.error = exception.getStatusCode().getReasonPhrase();
        this.errorDetails = exception.getErrorMessage();
        this.timestamp = DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now());
    }
}
