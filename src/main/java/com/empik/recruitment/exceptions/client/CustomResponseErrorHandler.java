package com.empik.recruitment.exceptions.client;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class CustomResponseErrorHandler extends DefaultResponseErrorHandler {

    @Override
    public void handleError(ClientHttpResponse clientResponse) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(clientResponse.getBody()))) {
            String errorMessage = buffer.lines().collect(Collectors.joining(""));
            throw new CustomClientResponseException(clientResponse.getStatusCode(), errorMessage);
        }
    }
}
