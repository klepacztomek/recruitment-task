package com.empik.recruitment.exceptions.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
@ToString
public class CustomClientResponseException extends RuntimeException {
    private HttpStatus statusCode;
    private String errorMessage;
}
