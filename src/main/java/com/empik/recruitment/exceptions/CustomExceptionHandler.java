package com.empik.recruitment.exceptions;

import com.empik.recruitment.exceptions.client.CustomClientResponseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class CustomExceptionHandler {

    @ExceptionHandler(value = CustomClientResponseException.class)
    public ResponseEntity<CustomErrorResponse> handleClientException(CustomClientResponseException clientException) {
        log.error("Client response exception: " + clientException.getErrorMessage());
        CustomErrorResponse error = new CustomErrorResponse(clientException);
        return ResponseEntity
                .status(error.getStatusCode())
                .body(error);
    }
}
