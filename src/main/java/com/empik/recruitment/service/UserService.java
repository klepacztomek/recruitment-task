package com.empik.recruitment.service;

import com.empik.recruitment.model.dto.UserDto;
import com.empik.recruitment.webclient.github.GithubClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {
    private final GithubClient githubClient;

    public UserDto getUserByLogin(String login) {
        return githubClient.getGithubUserByLogin(login);
    }
}