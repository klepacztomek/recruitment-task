package com.empik.recruitment.service;

import com.empik.recruitment.repository.login_counter.LoginCounterDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class LoginCounterService {

    private final LoginCounterDao loginCounterDao;

    public void incrementLoginCounter(String login) {
        loginCounterDao.updateLoginCounter(login);
    }
}
