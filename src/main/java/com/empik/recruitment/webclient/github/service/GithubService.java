package com.empik.recruitment.webclient.github.service;

import org.springframework.stereotype.Service;

@Service
public class GithubService {

    public float performCalculations(int followers, int publicRepos) {
        return followers != 0 ? 6 / (float) followers * (2 + publicRepos) : 0;
    }
}
