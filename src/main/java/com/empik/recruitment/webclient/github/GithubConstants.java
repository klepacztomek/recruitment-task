package com.empik.recruitment.webclient.github;

public class GithubConstants {
    static final String GITHUB_URL = "https://api.github.com/";

    static final String USERS = "users/";
    static final String USERS_BY_LOGIN = USERS + "{login}";
}
