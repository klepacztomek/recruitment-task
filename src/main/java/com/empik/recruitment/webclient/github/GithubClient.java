package com.empik.recruitment.webclient.github;

import com.empik.recruitment.model.dto.UserDto;
import com.empik.recruitment.webclient.github.dto.GithubUser;
import com.empik.recruitment.webclient.github.service.GithubService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static com.empik.recruitment.webclient.github.GithubConstants.GITHUB_URL;
import static com.empik.recruitment.webclient.github.GithubConstants.USERS_BY_LOGIN;

@Component
@AllArgsConstructor
public class GithubClient {
    private final RestTemplate restTemplate;
    private final GithubService githubService;

    public UserDto getGithubUserByLogin(String login) {
        GithubUser githubUser = callGetMethod(
                USERS_BY_LOGIN,
                GithubUser.class,
                login);

        return UserDto.builder()
                .id(githubUser.getId())
                .login(githubUser.getLogin())
                .name(githubUser.getName())
                .type(githubUser.getType())
                .avatarUrl(githubUser.getAvatarUrl())
                .createdAt(githubUser.getCreatedAt())
                .calculations(githubService.performCalculations(
                        githubUser.getFollowers(),
                        githubUser.getPublicRepos()))
                .build();
    }

    private <T> T callGetMethod(String url, Class<T> responseType, Object... objects) {
        return restTemplate.getForObject(GITHUB_URL + url,
                responseType, objects);
    }
}
