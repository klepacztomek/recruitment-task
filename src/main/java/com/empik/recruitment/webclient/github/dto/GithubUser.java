package com.empik.recruitment.webclient.github.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.time.Instant;

@Getter
public class GithubUser {
    private int id;
    private String login;
    private String name;
    private String type;

    @JsonProperty("avatar_url")
    private String avatarUrl;

    @JsonProperty("created_at")
    private Instant createdAt;

    private int followers;

    @JsonProperty("public_repos")
    private int publicRepos;
}
