package com.empik.recruitment.controller;

import com.empik.recruitment.model.dto.UserDto;
import com.empik.recruitment.service.LoginCounterService;
import com.empik.recruitment.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;
    private final LoginCounterService loginCounterService;

    @GetMapping("/{login}")
    public UserDto getUserByLogin(@PathVariable String login) {
        UserDto userDto = userService.getUserByLogin(login);
        log.debug("User info: " + userDto);
        loginCounterService.incrementLoginCounter(login);
        return userDto;
    }
}
